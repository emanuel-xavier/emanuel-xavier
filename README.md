# Hi there, I'm Emanuel Xavier!

Welcome to my GitHub/Gitlab profile. I'm a passionate DevOps and Software Engineer with a strong background in backend development and cloud infrastructure. I'm dedicated to creating efficient and reliable software solutions through automation, continuous integration, and thoughtful system design.

- GitRepos: [Github](https://github.com/emanuel-xavier) & [Gitlab](https://gitlab.com/emanuel-xavier)
- Email: [emanueljx@outlook.com](mailto:emanueljx@outlook.com)
- Linkedin: [Emanuel Xavier](https://www.linkedin.com/in/emanuel-xavier)
- Discord: emanuel.x

## About Me
- 🌍 Based in Brazil.
- 🎓 Graduated with a Bachelor's degree in Computer Science from the Federal University of Ouro Preto.
- 💼 I have experience working as a Backend Developer, SRE, DevOps Engineer, and Frontend Developer.
- 🛠️ I'm proficient in C++, Python, JavaScript, and Golang, with experience in backend development and automating tools.
- ☁️ Skilled in building and maintaining cloud infrastructure using AWS, GCP, and Docker.
- 🔧 I'm a problem solver at heart, with expertise in Linux server administration and troubleshooting.

## Work Experience

**Backend Developer & SRE/DevOps**
**TerraLAB** • Trainee/Internship • Mar 2020 - Nov 2022

Skills: `Node.js`, `SQL`, `Firebase`, `Jest`, `AWS`, `GCP`, `Docker`, `Terraform`, `Ansible`, `GitLabCI`, `CI/CD`, `Jenkins`

Programming Languages: `JavaScript`, `go`, `rust(learning)`, `python`, `C`, `C++`

Featured Projects:
- [Segurança da Mulher](https://play.google.com/store/apps/details?id=com.ouvidoria.mobile&hl=pt_BR&gl=US):
  A collaborative mobile app aimed at providing a safe platform for women to report incidents anonymously. My goal was to create a backend and cloud infrastructure using Node.js, Firebase, AWS and Docker.

- [Terraplanner](https://terraplanner.org/):
  A collaborative effort focused on creating an exciting map-based project designed to generate routes and geographical tools, with data visualization from various locations across Brazil. I played a role in establishing efficient DevOps practices using Terraform, Ansible, GitLab CI/CD, AWS, and GCP.

**Frontend Developer & DevOps Professional**
**PetroSoft** • Nov 2022 - Present

Skills: `JavaScript`, `CSS`, `HTML`, `Liferay`, `CI/CD`, `Jenkins`, `Docker`, `Linux`, `Windows`

## Let's Connect
I'm enthusiastic about collaboration and continuous learning. You can connect with me through any channels.

Looking forward to connecting with fellow
